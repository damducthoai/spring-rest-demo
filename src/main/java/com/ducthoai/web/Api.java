package com.ducthoai.web;

import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by root on 7/24/2017.
 */
@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class Api {
    @GetMapping
    String doGet(){
        //language=JSON
        return "{\"msg\":true}";
    }
    //@PostMapping
    @RequestMapping(method = RequestMethod.POST)
    Person doPost(@RequestBody Person person) {
        return person;
    }
}
